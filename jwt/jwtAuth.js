const jwt= require('jsonwebtoken');

const jwtAuth = (req,res,next)=>{
    // if(req.body.username==undefined || req.body.password==undefined){
    //     res.status(500).send({error:"authentication failed"})
    // }
    let username = req.body.username;
    let password = req.body.password;
    // usaually we can relay on db query to check if data exist or not. add db query here 
    let resp = {
        user : username,
        pass : password
    }
    let token = jwt.sign(resp,"secret",{expiresIn:186000})
    
    res.cookie('jwtToken','token',{signed:true,maxAge:Date.now()*1000})

    // res.status(200).send({auth:true,token:token});

    let authHeader = req.headers.authorization;
    if(authHeader==undefined){
        res.status(401).send({error:"no token provided"})
    }
    let tokenHeader = authHeader.split(" ").pop()
    jwt.verify(tokenHeader,'secret',(err,decoded)=>{
        if(err){
            res.status(500).send({error:"access denied"})
        }
        else{
            return next()
            // res.send(decoded);
        }

    })
    

}
module.exports = jwtAuth;