const bodyParser = require('body-parser');
const express = require('express')
const cookieParser = require('cookie-parser')
const app = express()
const port = 3000
const cors = require('cors')
app.use(bodyParser.json())
app.use(cors())
app.use(cookieParser('12345-67890-09876-54321'))

// routers

const router = require('./routes/router')

// authentication routers

// const Auth = require('./basicAuth/auth');
const Auth = require('./jwt/jwtAuth');

app.use('/',Auth);

app.get('/',(req,res)=>{
    res.end("welcome to backend. You are authenticated")
})

app.use(router);

app.listen(port,()=>{
    console.log("Server is now running");
})