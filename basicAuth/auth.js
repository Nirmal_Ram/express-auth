const auth = require('basic-auth')

const authorize=async(req,res,next)=>{
    console.log("auth done")
    const username = 'admin'
    const password = '1234'
    if(!req.signedCookies.XSRFToken){
        const user = await auth(req)
        if(user && user.name.toLowerCase()=== username.toLowerCase() && user.pass=== password ){
            res.cookie('XSRFToken','admin',{signed:true , maxAge:Date.now()*3000})
            res.statusCode = 200;
            return next()
        }
        else{
            var err = new Error('Please login to continue')
            res.setHeader('WWW-Authenticate','Basic');
            err.status =401;
            return next(err)
            
        }
    }
    else{
        if(req.signedCookies.XSRFToken==='admin')
            return next()
    }
}
module.exports=authorize